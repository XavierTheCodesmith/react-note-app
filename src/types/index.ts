export interface IUser {
    username: string;
    id: number;
    user_id: string;
    note_score: number;
}
