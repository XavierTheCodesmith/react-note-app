import React from 'react';

const AppFooter: React.FC = () => (
    <section data-testid="footer" className="absolute bottom-0 w-full h-18">
        <h1>Footer</h1>
    </section>
);

export default AppFooter;
